from _typeshed import Incomplete


def git_default_push_remote_name():
    ...


def dvc_discover_ssh_remote(host,
                            remote: Incomplete | None = ...,
                            forward_ssh_agent: bool = ...,
                            dry: bool = ...,
                            force: bool = ...) -> None:
    ...


def main() -> None:
    ...
