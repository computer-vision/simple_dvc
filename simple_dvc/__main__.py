#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
from . import main as main_mod

main = main_mod.main


if __name__ == '__main__':
    """
    CommandLine:
        python ~/code/simple_dvc/simple_dvc/__main__.py
    """
    main()
