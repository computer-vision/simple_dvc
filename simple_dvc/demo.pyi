from _typeshed import Incomplete


def init_randomized_dvc_repo(demo_root, with_git: bool = ...):
    ...


def random_nested_paths(num: int = ..., rng: Incomplete | None = ...):
    ...
