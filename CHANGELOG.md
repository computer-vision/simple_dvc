# Changelog

We are currently working on porting this changelog to the specifications in
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Version 0.2.1 - Unreleased


## [Version 0.2.0] -

### Added
* Push support

### Fixed
* Workaround networkx issue for tests

### Changed
* Better demodata structures


## [Version 0.0.1] -

### Added
* Initial version
