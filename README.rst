Simple DVC
==========

|Pypi| |PypiDownloads| |ReadTheDocs| |GitlabCIPipeline| |GitlabCICoverage|

+-----------------+-------------------------------------------------------+
| Read the Docs   | http://simple-dvc.readthedocs.io/en/latest/           |
+-----------------+-------------------------------------------------------+
| Gitlab (main)   | https://gitlab.kitware.com/computer-vision/simple_dvc |
+-----------------+-------------------------------------------------------+
| Pypi            | https://pypi.org/project/simple_dvc                   |
+-----------------+-------------------------------------------------------+


This repo aims to simplify DVC to *just* a data versioning tool (ignoring all
of the experiment management stuff), as well as provide other simple tools for
easier local cache management.


This installs the CLI program ``sdvc`` which currently has the following help page:


.. code::

    usage: sdvc [-h] {add,pull,request,cache_dir,sidecars,validate_sidecar,registery} ...

    A DVC CLI That uses our simplified (and more permissive) interface.

    The main advantage is that you can run these commands outside a DVC repo as
    long as you point to a valid in-repo path.

    options:
      -h, --help            show this help message and exit

    commands:
      {add,pull,request,cache_dir,sidecars,validate_sidecar,registery}
                            specify a command to run
        add                 Add data to the DVC repo.
        pull                Pull data from a DVC remote.
        request             Like pull, but only tries to pull if the requested file doesn't exist.
        cache_dir           Print the cache directory
        sidecars            List all sidecars associated with a path.
        validate_sidecar    Validate that everything marked in a sidecar file looks ok.
        registery           A DVC CLI That uses our simplified (and more permissive) interface.




.. |Pypi| image:: https://img.shields.io/pypi/v/simple_dvc.svg
    :target: https://pypi.python.org/pypi/simple_dvc

.. |PypiDownloads| image:: https://img.shields.io/pypi/dm/simple_dvc.svg
    :target: https://pypistats.org/packages/simple_dvc

.. |ReadTheDocs| image:: https://readthedocs.org/projects/simple-dvc/badge/?version=latest
    :target: http://simple-dvc.readthedocs.io/en/latest/

.. |GitlabCIPipeline| image:: https://gitlab.kitware.com/computer-vision/simple_dvc/badges/main/pipeline.svg
    :target: https://gitlab.kitware.com/computer-vision/simple_dvc/-/jobs

.. |GitlabCICoverage| image:: https://gitlab.kitware.com/computer-vision/simple_dvc/badges/main/coverage.svg
    :target: https://gitlab.kitware.com/computer-vision/simple_dvc/commits/main
