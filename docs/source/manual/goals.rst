While using DVC I've had these observations:

DVC seems to query the cache more often then necessary. It does it in these cases:

* DVC add, I have no idea why that is necessary.  We should be able to just add
  it to the flat hashed directory structure. SimpleDVC should implement a fast
  way to do this.

* DVC push, I have no idea why that is necessary.  We should just rsync the
  files in the ssh or local case. Let rsync determine if the files exist on the
  server or not. For fsspec, the query might be necessary, but we should be
  able to start pushing while queries are happening.
