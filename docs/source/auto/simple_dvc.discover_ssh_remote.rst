simple\_dvc.discover\_ssh\_remote module
========================================

.. automodule:: simple_dvc.discover_ssh_remote
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:
