:gitlab_url: https://gitlab.kitware.com/computer-vision/simple_dvc

.. The large version wont work because github strips rst image rescaling. https://i.imgur.com/AcWVroL.png
    # TODO: Add a logo
    .. image:: https://i.imgur.com/PoYIsWE.png
       :height: 100px
       :align: left

Welcome to simple_dvc's documentation!
======================================

.. The __init__ files contains the top-level documentation overview
.. automodule:: simple_dvc.__init__
   :show-inheritance:

.. toctree::
   :maxdepth: 5

   simple_dvc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`