# Add requirements here, use the script for help (erotemic needs to publish this script)
# python ~/local/tools/supported_python_versions_pip.py pygments
# python ~/local/tools/supported_python_versions_pip.py ubelt
# python ~/local/tools/supported_python_versions_pip.py scriptconfig
# python ~/local/tools/supported_python_versions_pip.py toml
# python ~/local/tools/supported_python_versions_pip.py rich

ubelt>=1.3.3     ;                            python_version >= '3.6'    # Python 3.6+

scriptconfig>=0.7.9     ;                            python_version >= '3.6'    # Python 3.6+

rich>=12.4.4    ;                            python_version >= '3.8'    # Python 3.8+
rich>=11.2.0    ; python_version < '3.8' and python_version >= '3.6'    # Python 3.6 

packaging>=21.3

PyYAML>=6.0.1     ; python_version < '4.0'  and python_version >= '3.12'    # Python 3.12+
PyYAML>=6.0       ; python_version < '3.12' 

ruamel.yaml>=0.17.22

kwutil >= 0.2.3

argcomplete>=1.0

# xdev availpkg pandas --request_min=1.1.4
pandas>=2.1.1  ; python_version < '4.0'  and python_version >= '3.12'    # Python 3.12+
pandas>=1.5.0  ; python_version < '3.12' and python_version >= '3.11'    # Python 3.11
pandas>=1.3.5  ; python_version < '3.11' and python_version >= '3.10'  # Python 3.10
pandas>=1.4.0  ; python_version < '3.10' and python_version >= '3.9'   # Python 3.9
pandas>=1.4.0  ; python_version < '3.9'  and python_version >= '3.8'   # Python 3.8
pandas>=1.2.0  ; python_version < '3.8'  and python_version >= '3.7'   # Python 3.7.1
pandas>=1.0.0  ; python_version < '3.7'  and python_version >= '3.6'   # Python 3.6.1

cmd_queue>=0.1.18

dvc>=3.7.0
