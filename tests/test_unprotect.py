
def test_unprotect_case1():
    import ubelt as ub
    from simple_dvc import SimpleDVC

    # import pytest
    # pytest.skip()

    dvc_dpath = ub.Path.appdir('simple_dvc/tests/test_unprotect_case1/repo')
    dvc = SimpleDVC.demo(dpath=dvc_dpath, reset=True, with_git=0)

    fpath1 = dvc.dvc_root / 'test-set1/assets/asset_001.data'
    print(ub.cmd(f'ls -al {fpath1}').stdout)
    print(ub.cmd(f'ls -alL {fpath1}').stdout)

    data1 = fpath1.read_text()
    print(data1)

    fpath2 = dvc.dvc_root / 'test-set1/assets/asset_002.data'
    print('UNPROTECTED CALL1')

    # If running inside of pytest, stdout/stderr can interact weirdly with DVC,
    # so we need to set verbose=0 here. In the future we should try to fix
    # this.
    dvc.unprotect(fpath2, verbose=0)
    dvc_root = dvc.dpath
    print('UNPROTECTED CALL2')
    _ = ub.cmd(f'dvc unprotect {fpath2.relative_to(dvc_root)}', cwd=dvc_root, verbose=2, system=True)

    fpath2.write_text('new data')
    fpath2.read_text()

    print('ADD CALL3')
    _ = ub.cmd('dvc add test-set1/assets', cwd=dvc_root, verbose=2, system=True)

    new_data1 = fpath1.read_text()
    assert new_data1 == data1

    new_data2 = fpath2.read_text()
    assert new_data2 == 'new data'


def test_unprotect_case2():
    import ubelt as ub
    from simple_dvc import SimpleDVC
    # Build a simple fresh dvc repo

    # import pytest
    # pytest.skip()

    dvc_dpath = ub.Path.appdir('simple_dvc/tests/test_unprotect_case2/repo')
    dvc = SimpleDVC.demo(dpath=dvc_dpath, reset=True, with_git=0)

    dvc_root = dvc.dpath

    fpath1 = dvc_root / 'test-set1/assets/asset_001.data'
    print(ub.cmd(f'ls -al {fpath1}').stdout)
    print(ub.cmd(f'ls -alL {fpath1}').stdout)

    data1 = fpath1.read_text()
    print(data1)

    fpath2 = dvc_root / 'test-set1/assets/asset_002.data'
    _ = ub.cmd('dvc unprotect test-set1/assets --verbose', cwd=dvc_root, verbose=2)

    fpath2.write_text('new data')
    fpath2.read_text()

    _ = ub.cmd('dvc add test-set1/assets', cwd=dvc_root, verbose=2, system=True)

    new_data1 = fpath1.read_text()
    assert new_data1 == data1

    new_data2 = fpath2.read_text()
    assert new_data2 == 'new data'
